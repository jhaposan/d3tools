class Vehicle2 {
  constructor(make, model, color) {
      this.make = make;
      this.model = model;
      this.color = color;
  }

  async getName() {
      console.log('Start')
      await new Promise(resolve => setTimeout(resolve, 1000));
      console.log('End')
  }
}
(async () => {
  let car2 = new Vehicle2("Toyota", "Corolla", "Black");
  for (let i = 0; i < 10 ; i++){
    await car2.getName()
  }
})();
(async () => {
  let car2 = new Vehicle2("Toyota", "Corolla", "Black");
  for (let i = 0; i < 10 ; i++){
    await car2.getName()
  }
})();